import {Container} from 'aurelia-dependency-injection';
import {HttpClient} from 'aurelia-http-client';
import SpiffApiGatewayServiceSdkConfig from './spiffApiGatewayServiceSdkConfig';
import ClaimSpiffEntitlementsFeature from './claimSpiffEntitlementsFeature';
import ListPartnerRepsInfoWithAccountIdFeature from './listPartnerRepsInfoWithAccountIdFeature';
import ListActivePartnerRepsInfoWithAccountIdFeature from './listActivePartnerRepsInfoWithAccountIdFeature';
import ListSpiffEntitlementsAccountIdFeature from './listSpiffEntitlementsAccountIdFeature';
import UploadInvoiceForPartnerSaleRegistrationFeature from './uploadInvoiceForPartnerSaleRegistrationFeature';

/**
 * @class {DiContainer}
 */
export default class DiContainer {

    _container:Container;

    /**
     * @param {SpiffApiGatewayServiceSdkConfig} config
     */
    constructor(config:SpiffApiGatewayServiceSdkConfig) {

        if (!config) {
            throw 'config required';
        }

        this._container = new Container();

        this._container.registerInstance(SpiffApiGatewayServiceSdkConfig, config);
        this._container.autoRegister(HttpClient);

        this._registerFeatures();

    }

    /**
     * Resolves a single instance based on the provided key.
     * @param key The key that identifies the object to resolve.
     * @return Returns the resolved instance.
     */
    get(key:any):any {
        return this._container.get(key);
    }

    _registerFeatures() {

        this._container.autoRegister(ClaimSpiffEntitlementsFeature);
        this._container.autoRegister(ListPartnerRepsInfoWithAccountIdFeature);
        this._container.autoRegister(ListActivePartnerRepsInfoWithAccountIdFeature);
        this._container.autoRegister(ListSpiffEntitlementsAccountIdFeature);
        this._container.autoRegister(UploadInvoiceForPartnerSaleRegistrationFeature);

    }

}
