import {inject} from 'aurelia-dependency-injection';
import SpiffApiGatewayServiceSdkConfig from './spiffApiGatewayServiceSdkConfig';
import {HttpClient} from 'aurelia-http-client';
import PartnerRepInfoWebView from './partnerRepInfoWebView';

@inject(SpiffApiGatewayServiceSdkConfig, HttpClient)
class ListActivePartnerRepsInfoWithAccountIdFeature {

    _config:SpiffApiGatewayServiceSdkConfig;

    _httpClient:HttpClient;

    constructor(config:SpiffApiGatewayServiceSdkConfig,
                httpClient:HttpClient) {

        if (!config) {
            throw 'config required';
        }
        this._config = config;

        if (!httpClient) {
            throw 'httpClient required';
        }
        this._httpClient = httpClient;
    }

    /**
     * Get the Active Partner Reps Information
     * @param {string} accountId
     * @param {string} accessToken
     * @returns {Promise.<PartnerRepInfoWebView[]>} partnerRepInfoWebView
     */
    execute(accountId:string,
            accessToken:string):Promise<PartnerRepInfoWebView[]> {

        return this._httpClient
            .createRequest(`spiff-api-gateway/activepartnerrepinfo/${accountId}`)
            .asGet()
            .withBaseUrl(this._config.precorConnectApiBaseUrl)
            .withHeader('Authorization', `Bearer ${accessToken}`)
            .send()
            .catch(error => {
                console.log(error);
            })
            .then(response => {
                return Array.from(
                        response.content,
                        contentItem =>
                            new PartnerRepInfoWebView(
                                contentItem.userId,
                                contentItem.firstName,
                                contentItem.lastName,
                                contentItem.bankInfoExists,
                                contentItem.w9InfoExists,
                                contentItem.contactInfoExists
                            )
                    );

            });
    }
}

export default ListActivePartnerRepsInfoWithAccountIdFeature;