/**
 * @class {SpiffEntitlementWithPartnerRepInfoWebView}
 */
export default class SpiffEntitlementWithPartnerRepInfoWebView {

    _spiffEntitlementId:number;

    _partnerSaleRegistrationId:number;

    _installDate:string;

    _spiffAmount:number;

    _partnerRepUserId:string;

    _invoiceUrl:string;

    _invoiceNumber:string;

    _facilityName:string;

    _firstName:string;

    _lastName:string;

    _isBankInfoExists:boolean;

    _isW9InfoExists:boolean;

    _isContactInfoExists:boolean;

    _sellDate:string;


    /**
     * @param {number} spiffEntitlementId
     * @param {number} partnerSaleRegistrationId
     * @param {string} installDate
     * @param {number} spiffAmount
     * @param {string} partnerRepUserId
     * @param {string} invoiceUrl
     * @param {string} invoiceNumber
     * @param {string} facilityName
     * @param {string} firstName
     * @param {string} lastName
     * @param {boolean} isBankInfoExists
     * @param {boolean} isW9InfoExists
     * @param {boolean} isContactInfoExists
     * @param {string} sellDate
     */
    constructor(spiffEntitlementId:number,
                partnerSaleRegistrationId:number,
                installDate:string,
                spiffAmount:number,
                partnerRepUserId:string,
                invoiceUrl:string,
                invoiceNumber:string,
                facilityName:string,
                firstName:string,
                lastName:string,
                isBankInfoExists:boolean,
                isW9InfoExists:boolean,
                isContactInfoExists:boolean,
                sellDate:string
                ) {

        if (!spiffEntitlementId) {
            throw new TypeError('spiffEntitlementId required');
        }
        this._spiffEntitlementId = spiffEntitlementId;

        if (!partnerSaleRegistrationId) {
            throw new TypeError('partnerSaleRegistrationId required');
        }
        this._partnerSaleRegistrationId = partnerSaleRegistrationId;
        
        this._installDate = installDate;

        this._spiffAmount = spiffAmount;

        if (!invoiceNumber) {
            throw new TypeError('invoiceNumber required');
        }
        this._invoiceNumber = invoiceNumber;

        if (!facilityName) {
            throw new TypeError('facilityName required');
        }
        this._facilityName = facilityName;

        this._invoiceUrl = invoiceUrl;

        this._partnerRepUserId = partnerRepUserId;

        this._firstName = firstName;

        this._lastName = lastName;

        this._isBankInfoExists = isBankInfoExists;

        this._isW9InfoExists = isW9InfoExists;

        this._isContactInfoExists = isContactInfoExists;

        this._sellDate = sellDate;
    }

    /**
     * @returns {number}
     */
    get spiffEntitlementId():number {
        return this._spiffEntitlementId;
    }

    /**
     * @returns {number}
     */
    get partnerSaleRegistrationId():number {
        return this._partnerSaleRegistrationId;
    }

    /**
     * @returns {string}
     */
    get installDate():string {
        return this._installDate;
    }

    /**
     * @returns {number}
     */
    get spiffAmount():number {
        return this._spiffAmount;
    }

    /**
     * @returns {string}
     */
    get partnerRepUserId():string {
        return this._partnerRepUserId;
    }

    /**
     * @returns {string}
     */
    get invoiceUrl():string {
        return this._invoiceUrl;
    }

    /**
     * @returns {string}
     */
    get invoiceNumber():string {
        return this._invoiceNumber;
    }

    /**
     * @returns {string}
     */
    get facilityName():string {
        return this._facilityName;
    }

    /**
     * @returns {string}
     */
    get firstName():string {
        return this._firstName;
    }

    /**
     * @returns {string}
     */
    get lastName():string {
        return this._lastName;
    }

    /**
     * @returns {boolean}
     */
    get isBankInfoExists():boolean {
        return this._isBankInfoExists;
    }

    /**
     * @returns {boolean}
     */
    get isW9InfoExists():boolean {
        return this._isW9InfoExists;
    }

    /**
     * @returns {boolean}
     */
    get isContactInfoExists():boolean {
        return this._isContactInfoExists;
    }

    get sellDate():string{
        return this._sellDate;
    }

    toJSON() {
        return {
            spiffEntitlementId:this._spiffEntitlementId,
            partnerSaleRegistrationId: this._partnerSaleRegistrationId,
            installDate: this._installDate,
            spiffAmount: this._spiffAmount,
            partnerRepUserId: this._partnerRepUserId,
            invoiceUrl: this._invoiceUrl,
            invoiceNumber: this._invoiceNumber,
            facilityName: this._facilityName,
            firstName: this._firstName,
            lastName: this._lastName,
            isBankInfoExists: this._isBankInfoExists,
            isW9InfoExists: this._isW9InfoExists,
            isContactInfoExists: this._isContactInfoExists,
            sellDate:this._sellDate
        };
    }
}