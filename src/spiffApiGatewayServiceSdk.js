import SpiffApiGatewayServiceSdkConfig from './spiffApiGatewayServiceSdkConfig';
import DiContainer from './diContainer';
import ClaimSpiffWebView from './claimSpiffWebView';
import PartnerRepInfoWebView from './partnerRepInfoWebView';
import PartnerSaleInvoiceWebView from './partnerSaleInvoiceWebView';
import SpiffEntitlementWithPartnerRepInfoWebDto from './spiffEntitlementWithPartnerRepInfoWebDto';
import SpiffEntitlementWithPartnerRepInfoWebView from './spiffEntitlementWithPartnerRepInfoWebView';
import UploadPartnerSaleInvoiceReqWebDto from './uploadPartnerSaleInvoiceReqWebDto';
import ClaimSpiffEntitlementsFeature from './claimSpiffEntitlementsFeature';
import ListPartnerRepsInfoWithAccountIdFeature from './listPartnerRepsInfoWithAccountIdFeature';
import ListActivePartnerRepsInfoWithAccountIdFeature from './listActivePartnerRepsInfoWithAccountIdFeature';
import ListSpiffEntitlementsAccountIdFeature from './listSpiffEntitlementsAccountIdFeature';
import UploadInvoiceForPartnerSaleRegistrationFeature from './uploadInvoiceForPartnerSaleRegistrationFeature';

/**
 * @class {SpiffApiGatewayServiceSdk}
 */
export default class SpiffApiGatewayServiceSdk {

    _diContainer:DiContainer;

    /**
     * @param {SpiffApiGatewayServiceSdkConfig} config
     */
    constructor(
        config:SpiffApiGatewayServiceSdkConfig
    ) {

        this._diContainer
            = new DiContainer(config);
    }

    /**
     * Post the spiff entitlements with partnerRep info
     * @param {string} accountId
     * @param {SpiffEntitlementWithPartnerRepInfoWebDto[]} request
     * @param {string} accessToken
     * @returns {Promise.<ClaimSpiffWebView[]>} claimSpiffWebViews
     */
    claimSpiffEntitlements(accountId:string,
                           request:SpiffEntitlementWithPartnerRepInfoWebDto[],
                           accessToken:string):Promise<ClaimSpiffWebView[]> {

        return this
            ._diContainer
            .get(ClaimSpiffEntitlementsFeature)
            .execute(
                accountId,
                request,
                accessToken
            );

    }

    /**
     * Get the partner Reps Information
     * @param {string} accountId
     * @param {string} accessToken
     * @returns {Promise.<PartnerRepInfoWebView[]>} partnerRepInfoWebView
     */
    listPartnerRepsInfoWithAccountId(accountId:string,
                                     accessToken:string):Array<PartnerRepInfoWebView> {

        return this
            ._diContainer
            .get(ListPartnerRepsInfoWithAccountIdFeature)
            .execute(
                accountId,
                accessToken
            );

    }

    /**
     * Get the Active Partner Reps Information
     * @param {string} accountId
     * @param {string} accessToken
     * @returns {Promise.<PartnerRepInfoWebView[]>} partnerRepInfoWebView
     */
    listActivePartnerRepsInfoWithAccountId(accountId:string,
                                     accessToken:string):Array<PartnerRepInfoWebView> {

        return this
            ._diContainer
            .get(ListActivePartnerRepsInfoWithAccountIdFeature)
            .execute(
                accountId,
                accessToken
            );

    }

    /**
     * Get the spiff entitlements
     * @param {string} accountId
     * @param {string} accessToken
     * @returns {Promise.<SpiffEntitlementWithPartnerRepInfoWebView[]>} spiffEntitlementWithPartnerRepInfoWebView
     */
    listSpiffEntitlementsAccountId(accountId:string,
                                   accessToken:string):Array<SpiffEntitlementWithPartnerRepInfoWebView> {

        return this
            ._diContainer
            .get(ListSpiffEntitlementsAccountIdFeature)
            .execute(
                accountId,
                accessToken
            );

    }

    /**
     * Post the upload partnerSale invoice
     * @param {UploadPartnerSaleInvoiceReqWebDto} request
     * @param {string} accessToken
     * @returns {Promise.<PartnerSaleInvoiceWebView>} partnerSaleInvoiceWebView
     */
    uploadInvoiceForPartnerSaleRegistration(
        request:UploadPartnerSaleInvoiceReqWebDto,
        accessToken:string):PartnerSaleInvoiceWebView {

        return this
            ._diContainer
            .get(UploadInvoiceForPartnerSaleRegistrationFeature)
            .execute(
                request,
                accessToken
            );
    }

}
